import "./menu.css";

function Menu() {
  return (
    <div className="menu">
      <div className="menu-button menu-button-selected">TEAM MEMBERS</div>
      <div className="menu-button">PRODUCTS</div>
      <div className="menu-button">INDUSTRY</div>
    </div>
  );
}

export default Menu;
