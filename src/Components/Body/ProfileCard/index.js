import "./profileCard.css";

function ProfileCard(props) {
  return (
    <div className="profile-card">
      <div className="profile-card__initials">{props.person.initials}</div>
      <div className="profile-card__name">{props.person.name}</div>
      <div className="profile-card__handle">{props.person.handle}</div>
      <hr />
      <div>
        <div className="profile-card__trends">
          {/* insert icon here */}
          {props.person.trend}
        </div>
        <div className="profile-card__num-of-sales">
          {props.person.numOfSales}
        </div>
      </div>
      <div className="profile-card__bottom">
        <div className="profile-card__buttons">Online</div>
        <div className="profile-card__buttons">Profile</div>
      </div>
    </div>
  );
}

export default ProfileCard;
