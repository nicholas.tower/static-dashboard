import "./header.css";
import Avatar from "./Avatar";

function Header() {
  return (
    <div className="header">
      <div className="title">
        <span className="bigger-text">Current Sales</span>

        <span>By team member</span>
      </div>

      <Avatar />
    </div>
  );
}

export default Header;
