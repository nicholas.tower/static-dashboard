import "./avatar.css";

function Avatar() {
  return (
    <div className="avatar">
      <img className="image" src="" alt="img" />
      <div>Hello, master Skywalker!</div>
    </div>
  );
}

export default Avatar;
